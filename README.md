# SSO Tester

OIDC client API : https://github.com/panva/node-openid-client/blob/main/docs/README.md

## Ory Hydra configuration

```bash
hydra create oauth2-client --grant-type authorization_code,refresh_token --name sso-tester --redirect-uri https://sso-tester.agepoly.ch/sso/callback --
response-type code,id_token --scope openid,email,profile,groups -e http://localhost:4445

hydra update oauth2-client d1f8020c-2f43-4e38-bf90-5cb7d1ca81bf --grant-type authorization_code,refresh_token --name sso-tester --redirect-uri https://
sso-tester.agepoly.ch/sso/callback,http://localhost:4000/sso/callback --response-type code,id_token --scope openid,email,profile,groups -e http://localhost
:4445
```

```
CLIENT ID       d1f8020c-2f43-4e38-bf90-5cb7d1ca81bf
CLIENT SECRET
GRANT TYPES     authorization_code, refresh_token
RESPONSE TYPES  code, id_token
SCOPE           openid email profile groups
AUDIENCE
REDIRECT URIS   https://sso-tester.agepoly.ch/sso/callback, http://localhost:4000/sso/callback

```