import 'dotenv/config'
import express from 'express';
import { Issuer, generators } from 'openid-client';

const production = process.env.ENV !== 'dev'
const port = process.env.EXPRESS_PORT || 4000
const oidcDiscoveryUrl = 'https://hydra.agepoly.ch/.well-known/openid-configuration'
const defaultCallbackUrl = production ? 'https://sso-tester.agepoly.ch/sso/callback' : 'http://localhost:4000/sso/callback'
const callbackUrl = process.env.CALLBACK_URL || defaultCallbackUrl

console.warn(`PRODUCTION ENVIRONMENT = ${production}`)

const app = express()
const oidcIssuer = await Issuer.discover(oidcDiscoveryUrl)
const codeVerifier = generators.codeVerifier()
const codeChallenge = generators.codeChallenge(codeVerifier)
const state = 'thisismysueprstate'

// console.log('Discovered issuer %s %O', oidcIssuer.issuer, oidcIssuer.metadata);

const client = new oidcIssuer.Client({
    client_id: process.env.SSO_CLIENT_ID,
    client_secret: process.env.SSO_CLIENT_SECRET,
    redirect_uris: [ callbackUrl ],
    response_types: ['code']
})

app.get('/', (req, res) => {
    const authorizationUrl = client.authorizationUrl({
        scope: 'openid email profile groups',
        code_challenge: codeChallenge,
        code_challenge_method: 'S256',
        state,
    })
    console.log(authorizationUrl)

    res.send(`Hello world! <br /> <a href="${authorizationUrl}">Authorize</a>`)
})

app.get('/sso/callback', async (req, res) => {
    const params = client.callbackParams(req)
    
    try {
        const tokenSet = await client.callback(callbackUrl, params, { code_verifier: codeVerifier, state })
        const accessToken = tokenSet.access_token
        const userinfo = await client.userinfo(accessToken);
        
        const obj = {
            accessToken,
            idToken: tokenSet.id_token ?? null,
            claims: tokenSet.claims(),
            userinfo,
        }
        console.log(obj)
        res.json(obj)
    } catch (e) {
        res.status(400).send(e.toString() + '<br /><a href="/">Back</a>')
    }
})

process.on('SIGTERM', shutDown);
process.on('SIGINT', shutDown);

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})

function shutDown() {
    console.log('Received kill signal, shutting down gracefully');
    process.exit(0)
}