FROM node:alpine as base

WORKDIR /app

COPY package.json package-lock.json ./

RUN rm -rf node_modules && npm install && npm cache clean --force

COPY . .

CMD ["node", "./index.js"]